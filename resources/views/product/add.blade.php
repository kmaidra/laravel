<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-blue-900 leading-tight">
            {{ __('Add Products') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-screen-md mx-auto sm:px-6 lg:px-8">
            <form method="POST" action="{{ route('product.submit') }}" enctype="multipart/form-data">
                    @csrf
                    <div>
                        <x-label for="name" :value="__('Name')" />
                        <x-input id="" class="block mt-1 w-full" type="text" name="name" :value="old('name')" />

                        <x-label for="price" :value="__('Price')" />
                        <x-input id="" class="block mt-1 w-full" type="text" name="price" :value="old('price')" />

                        <select class="rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="category" id="category">
                            @foreach ($category as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                        <div class="mt-1">                            
                            <label class="text-sm" for="image">Image</label>
                            <input id="" class="block mt-1 w-full" type="file" name="image" />
                        </div>
                        @error('image')
                            <div class="text-red-500 text-sm mt-1">{{ $message }}</div>
                        @enderror
                        <div class="mt-1 flex flex-col">
                            <label class="text-sm" for="description">Description</label>
                            <textarea class=" shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="description" id="desc" cols="30" rows="5"></textarea>
                        </div>
                        @error('description')
                            <div class="text-red-500 text-sm mt-1">{{ $message }}</div>
                        @enderror
                    </div>
                    <x-button class="ml-3 mt-2">
                        {{ __('Submit') }}
                    </x-button>
            </form>
        </div>
    </div>
    <x-flash />
</x-app-layout>