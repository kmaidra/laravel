{{-- <x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-blue-900 leading-tight">
            {{ __('Products') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <a class="py-2 px-3 text-white bg-gray-800 rounded" href="{{route('product.add')}}">Add new</a>
                </div>
            </div>
            <!-- <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg mt-3">
                <div class="p-6 bg-white border-b border-gray-200">
                    @foreach ($products as $product)
                        <p class="text-sm font-semibold">{{$product->name}}</p>
                        <a href="{{ route('product.edit', ['id' => $product->id]) }}" class="btn btn-success btn-sm">Edit</a>
                        <form action="{{ route('product.destroy', ['id' => $product->id]) }}" method="POST" class="d-inline-block">
                        @csrf
                        <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                        </form>
                    @endforeach
                </div>
            </div> -->
        </div>
    </div>
</x-app-layout> --}}


<x-app-layout>

    <x-slot name="header">
        <h2 class="font-semibold text-xl text-blue-900 leading-tight">
            {{ __('Products') }}
        </h2>
    </x-slot>

     <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
        <div class="p-6 bg-white border-b border-gray-200">
            <a class="py-2 px-3 text-white bg-gray-800 rounded" href="{{route('product.add')}}">Add new</a>
        </div>
    </div>

    <table class="table table-hover">
        <thead>
        <tr class="table-dark">
            <th>#</th>
            <th>Image</th>
            <th>Created At</th>
            <th>Name</th>
            <th>Price</th>
            <th>Category</th>
            <th>Description</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>

        @foreach($products as $product)
            <tr>
                <th scope="row">{{$product->id}}</th>
                <td class="text-center"><img class="h-12" src="{{$product->image}}" alt=""></td>
                <td>{{ $product->created_at }}</td>
                <td>{{ $product->name }}</td>
                <td>{{ $product->price }}</td>
                <td>{{ $product->category->name }}</td>
                <td>{{ $product->description }}</td>
                <td class="flex gap-2">
                    <a href="{{ route('product.edit', ['id' => $product->id]) }}" class="btn btn-success btn-sm">Edit</a>
                    <form action="{{ route('product.delete', ['id' => $product->id]) }}" method="POST" class="d-inline-block">
                        @csrf
                        <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>
    <x-flash />
</x-app-layout>
