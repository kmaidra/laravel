<x-app-layout>
    
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-blue-900 leading-tight">
            {{ __('Edit Products') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-screen-md mx-auto sm:px-6 lg:px-8">
            
            <form action="{{ route('product.update', ['id' => $product->id]) }}" method="POST" enctype="multipart/form-data">

                @csrf
                <div>
                    <div>
                        <input type="hidden" name="id" value="{{$product->id}}">
                        <label>Name</label>
                        <input type="text" name="name" class="block font-medium text-sm text-gray-700" placeholder="product name" value="{{old('name') ?? $product->name}}">
                    </div>
                    @error('name')
                        <div class="text-red-500 text-sm mt-1">{{ $message }}</div>
                    @enderror
                    <div class="form-group">
                        <label>Price</label>
                        <input class="block font-medium text-sm text-gray-700" type="text" name="price" value="{{old('price') ?? $product->price}}">
                    </div>
                    @error('price')
                        <div class="text-red-500 text-sm mt-1">{{ $message }}</div>
                    @enderror
                    <select class="rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="category" id="">
                        @foreach ($category as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>                        
                        @endforeach
                    </select>
                    <div class="mt-1">                            
                        <label class="text-sm" for="image">Image</label>
                        <input id="" class="block mt-1 w-full" type="file" name="image" value="{{old('image') ?? $product->image}}" />
                    </div>
                    @error('image')
                        <div class="text-red-500 text-sm mt-1">{{ $message }}</div>
                    @enderror
                    <div class="flex flex-col">
                        <label class="text-sm" for="description">Description</label>
                        <textarea  value="{{old('description') ?? $product->description}}" class="shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="description" id="desc" cols="30" rows="5">{{old('description') ?? $product->description}}</textarea>
                    </div>
                    @error('description')
                        <div class="text-red-500 text-sm mt-1">{{ $message }}</div>
                    @enderror
                    </div>
                    <x-button class="ml-3 mt-2">
                        {{ __('Submit') }}
                    </x-button>
                </div>
            </form>
        </div>
    </div>
    <x-flash />
</x-app-layout>