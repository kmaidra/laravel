@props([
    'cart'
])
<div class="flex items-center justify-between px-6 bg-opacity-70 h-full w-full">
    <div class="bg-gray-800 h-full px-4 flex flex-col justify-center">
        <h1 class="text-xl text-white font-bold">Commerce</h1>
        <p class="text-xs text-gray-400">Some random quote</p>
    </div>
    <div class="flex items-center gap-4">
    <x-cart-button :cart="$cart" />
        @if (Route::has('login'))
        @auth
        <a href="{{ url('/dashboard') }}" class="text-sm text-slate-700 underline">Dashboard</a>
        @else
        <a href="{{ route('login') }}" class="text-sm text-slate-700 underline">Log in</a>

        @if (Route::has('register'))
        <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Register</a>
        @endif
        @endauth
        @endif
    </div>
</div>