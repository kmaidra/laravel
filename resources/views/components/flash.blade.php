@if(session()->has('message'))
        <div class="flex justify-center">
            <div x-data="{ show: true }"
                 x-init="setTimeout(() => show = false, 3000)"
                 x-show="show"
                 class="bg-blue-800 text-white py-4 px-8 rounded-md">
                {{ session()->get('message') }}
            </div>
        </div>
@endif