<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <!-- <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script> -->
    </head>
    <body>
        <div class="font-sans min-h-screen max-w-screen-xl mx-auto w-full text-gray-900 antialiased">
            <x-nav :cart="$cart" />
            {{ $slot }}
            <footer class="h-full border-t-2 mx-6 border-teal-800 grid grid-cols-3">
                <div class="flex gap-2 mt-4">
                    <div class="h-8 w-8 text-teal-800">
                        <x-svg.youtube />
                    </div>
                    <div class="h-8 w-8 text-teal-800">
                        <x-svg.instagram />
                    </div>
                </div>
                <div class="hidden lg:flex justify-center">
                    <div>
                        <h1 class="text-white bg-gray-800 px-2 py-1 text-base font-bold">Commerce</h1>
                    </div>
                </div>
                <p class="text-sm mt-4 justify-self-end text-gray-500">Copyright 2022 by Commerce</p>
            </footer>
        </div>
    </body>

</html>