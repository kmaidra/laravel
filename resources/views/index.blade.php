<x-index-layout>
    <div class="max-w-screen-xl px-12 mx-auto w-full h-full mt-16">
        <div class="grid md:grid-cols-2">
            <div class="h-full">
                <img class="aspect-[4/3] h-full object-cover" src="https://images.unsplash.com/photo-1557821552-17105176677c?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1332&q=80" alt="picture">
            </div>
            <div class="bg-gray-800 px-6 space-y-4 flex flex-col pt-4">
                <h1 class="text-2xl font-bold text-teal-100">Lorem ipsum dolor sit amet</h1>
                <p class="mt-4 max-w-[50ch] text-lg text-white"> Lorem ipsum dolor, sit amet consectetur adipisicing elit. Unde iste, dolorum veniam natus velit quisquam corporis? Aut ipsa repellat corrupti quam delectus commodi molestias voluptatibus debitis perspiciatis doloremque. Ad, eius?</p>
                <p class="mt-4 max-w-[50ch] text-lg text-white">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ab suscipit modi, id, dignissimos nulla labore asperiores exercitationem, voluptates aliquam laudantium in hic recusandae quasi! Ad soluta fugiat libero modi blanditiis?</p>
                <div class="flex-1 flex pb-8">
                    <a href="#" class="text-white self-end px-2 py-1 text-sm uppercase font-bold"></a>
                </div>
            </div>
        </div>
        <div class="border-teal-800 mt-6 border-t-2 flex justify-center">
            <h1 class="font-bold bg-gray-800 text-white px-2 mb-8">Products</h1>
        </div>
        
    </div>
    <div class="grid mt-4 pb-12 gap-6 grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 place-items-center">
        @foreach ($products as $product)
        <x-product :product="$product"/>
        @endforeach
    </div>

    </x-index-layout>

    <script>
        const submitData = (e, form) => axios.post(`cart-add/${e}`, form)
            .then(res => {
                window.location.reload()
            })
    </script>