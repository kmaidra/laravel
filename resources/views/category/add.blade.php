<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-blue-900 leading-tight">
            {{ __('Add Categories') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-screen-md mx-auto sm:px-6 lg:px-8">
            <form method="POST" action="{{ route('category.submit') }}">
                    @csrf
                    <div>
                        <x-label for="name" :value="__('Name')" />
                        <x-input id="" class="block mt-1 w-full" type="text" name="name" :value="old('name')" />

                        <x-button class="ml-3 mt-3">
                            {{ __('Submit') }}
                        </x-button>
                    </div>
                    {{-- <div class="py-12">
                        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                                <div class="p-6 bg-white border-b border-gray-200">
                                    @foreach ($categories as $category)
                                        <p class="text-sm font-semibold">{{$category->name}}</p>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div> --}}
            </form>
        </div>
    </div>
</x-app-layout>



