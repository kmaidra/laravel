<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {       
        return view('category/add', [
            'categories' => Category::all()
        ]);
    }
    public function create(){
        return view('category/add');
    } 

    public function store(Request $request)
    {
        Category::create($request->validate([
            'name' => 'required'
        ]));
        return redirect()->back();
    }

}


