<?php
namespace App\Http\Controllers;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
class ProductController extends Controller
{
    
    public function index()
    {
        $products = Product::with('category')->get();
        return view('product/index', compact('products'));
        
    }
    public function create()
    {
        return view('product/add', [
            'category' => Category::all()
        ]);
    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'price' => 'required|numeric',
            'category' => 'required',
            'description' => 'required',
            'image' => 'required'
        ]);

        $file = $request->file('image');
        $path = Storage::putFile('images', $file);
        
        Product::create([
            'name' => $request->name,
            'price' => $request->price,
            'description' => $request->description,
            'category_id' => $request->category,
            'image' => $path
        ]);
        return redirect()->back()->with('message', 'Product added');
    }
   
    public function edit(Request $request, $id)
    {
        $category = Category::all();
        $product = Product::findOrFail($id);
        
        // dd($product);
        return view('product/edit',[
            'product' => $product,
            'category' => $category
        ]);
    }
    
    public function update(Request $request)
    {
        $product = Product::find($request->id);
        $request->validate([
            'name' => 'required',
            'price' => 'required|numeric',
            'category' => 'required',
            'description' => 'required',
        ]);
        
        $file = $request->file('image');
        $path = $product->image;
        if(!empty($file)){
            Storage::delete($product->image);
            $path = Storage::putFile('images', $file);
        }
        $product->update([
            'name' => $request->name,
            'price' => $request->price,
            'description' => $request->description,
            'category_id' => $request->category,
            'image' => $path
        ]);
        return redirect()->route('product.index')->with('message', 'Product successfully updated.');
    }
   
    public function show(Product $product)
    {
        //
    }
   
    public function destroy(Request $request)
    {
        $product = Product::find($request->id);
        Storage::delete($product->image);
        $product->delete();
    
        return redirect()->route('product.index')
                        ->with('message','Product deleted successfully');
    }
    // public function cart()
    // {
    //     return view('cart');
    // }
}