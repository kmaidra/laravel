<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index() 
    {
        return view('dashboard', [
            'users' => User::all()
        ]);
    }

    // public function index()
    // {       
    //     return view('dashboard', [
    //         'categories' => Category::all()
    //     ]);
    // }

}


