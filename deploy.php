<?php
namespace Deployer;

require 'recipe/laravel.php';

// Config


set('application', 'backend_Ralf');
set('remote_user', 'virt98409');
set('http_user', 'virt98409');
set('keep_releases', 2);

set('repository', 'git@gitlab.com:kmaidra/laravel.git');


// Hosts

host('ta20maidra.itmajakas.ee')
    ->setHostname('ta20maidra.itmajakas.ee')
    ->set('http_user', 'virt98409')
    ->set('deploy_path','~/domeenid/www.ta20maidra.itmajakas.ee/epood')
    ->set('branch', 'main');


    task('opcache:clear', function () {
        run('killall php80-cgi || true');
    })->desc('Clear opcache');

    task('build:node', function () {
        cd('{{release_path}}');
        run('npm i');
        run('npm run prod');
        run('rm -rf node_modules');
    });
    task('deploy', [
        'deploy:prepare',
        'deploy:vendors',
        'artisan:storage:link',
        'artisan:view:cache',
        'artisan:config:cache',
        'build:node',
        'deploy:publish',
        'opcache:clear',
        'artisan:cache:clear'
    ]);
    after('deploy:failed', 'deploy:unlock');
