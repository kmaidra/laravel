<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ShopController;
use Illuminate\Support\Facades\Route;


Route::get('/', [ShopController::class, 'index']);
Route::get('cart', [ShopController::class, 'cart'])->name('cart.index');
Route::post('cart-add/{product}', [ShopController::class, 'cartAdd'])->name('cart.add');
Route::post('cart-update/{product}', [ShopController::class, 'cartUpdate'])->name('cart.update');
Route::post('cart-delete/{product}', [ShopController::class, 'destroy'])->name('cart.delete');
Route::post('/pay', [ShopController::class, 'pay'])->name('pay');
Route::post('success', [ShopController::class, 'success'])->name('success');

Route::middleware('auth')->group(function() {
    Route::get('/dashboard', [AdminController::class, 'index'])->name('dashboard');
    Route::get('/profile', [AdminController::class, 'index'])->name('profile');

    //product
    Route::get('/products', [ProductController::class, 'index'])->name('product.index');
    Route::get('/product-add', [ProductController::class, 'create'])->name('product.add');
    Route::get('/product-edit/{id}', [ProductController::class, 'edit'])->name('product.edit');
    Route::post('/product-update', [ProductController::class, 'update'])->name('product.update');
    Route::post('/product-submit', [ProductController::class, 'store'])->name('product.submit');
    Route::post('/product-delete', [ProductController::class, 'destroy'])->name('product.delete');
    //category
    Route::get('/categories', [CategoryController::class, 'index'])->name('category.index');
    Route::get('/category-add', [CategoryController::class, 'create'])->name('category.add');
    Route::post('/category-submit', [CategoryController::class, 'store'])->name('category.submit');
});

require __DIR__.'/auth.php';
